import de.lordsalmon.dev.icn.backend.api.IcnAPI;
import de.lordsalmon.dev.icn.backend.api.config.ConfigService;
import de.lordsalmon.dev.icn.backend.api.config.DataService;
import de.lordsalmon.dev.icn.backend.api.exceptions.ConfigIncompleteException;
import org.json.JSONObject;

import java.util.logging.Logger;

public class TestPlugin extends IcnAPI {

    public static final String PLUGIN_VERSION = "1.0.3";
    public static final String LATEST_MESSAGE = "now chained accessors";
    public static final String DISPLAY_NAME = "Test - (inactive)";

    public IcnAPI ICNInstance = IcnAPI.getInstance(TestPlugin.class);
    
    public void initialize() {
        ICNInstance.setConfigService(ConfigService.getInstance(TestPlugin.class))
                .setLogger(Logger.getLogger(TestPlugin.class.getSimpleName()))
                .setDataService(DataService.getInstance(TestPlugin.class));
        try {
            ICNInstance.initTelegram();
        } catch (ConfigIncompleteException e) {
            e.printStackTrace();
        }
    }

    public void call(JSONObject args, JSONObject componentBase) {
        System.out.println(args.toString());
        System.out.println("testplugion has been called");
    }

}